// ================================================================================
// name: tdetectorhfc~.c
// desc: extern for pd
//          simple test that just outputs the input signal
//
//
//
// fft code at bottom from musicdsp.org by Toth Laszlo
// see: http://musicdsp.org/files/rvfft.cpp and http://musicdsp.org/files/rvfft.ps
// adjusted for floats and for pi
//
//
// author: jennifer hsu
// date: fall 2013
// =================================================================================

#include "m_pd.h"
#include <stdlib.h>
#include <math.h>


// struct type to hold all variables that have to do with the delayline~ object
typedef struct _tdetectorhfc
{
    t_object x_obj;
    t_float sr;
    t_float * fftBuffer;
    t_float lastHFC;
    t_float thresh;
    t_int numFrames;
    t_outlet * bangOutlet;
    t_int framesSinceLastTransient;
} t_tdetectorhfc;

// tdetectorhfc~ class that will be created in 'setup'
// and used in 'new' to create new instances
t_class *tdetectorhfc_class;

/* function prototypes */
static t_int *tdetectorhfc_perform(t_int *w);
static void tdetectorhfc_dsp(t_tdetectorhfc *x_obj, t_signal **sp);
void tdetectorhfc_setNumFrames(t_tdetectorhfc *x, t_floatarg f);
static void *tdetectorhfc_new(void);
void tdetectorhfc_tilde_setup(void);
// FFT functions
void realfft_split(float *data,long n);
void realfft_split_unshuffled(float *data,long n);
void irealfft_split(float *data,long n);


// perform function (audio callback)
static t_int *tdetectorhfc_perform(t_int *w)
{
    
    // extract data
    t_tdetectorhfc *x = (t_tdetectorhfc *)(w[1]);
    t_float *in = (t_float *)(w[2]);
    int bufferSize = (int)(w[3]);
    
    int sample;
    // copy into delay buffer at correct point
    for(sample = 0; sample < bufferSize; sample++)
    {
        // fill in FFT buffer with input samples
        // window this at another point
        *(x->fftBuffer+sample) = *(in+sample);
    }
    
    // take the FFT for this block
    realfft_split_unshuffled(x->fftBuffer, bufferSize);

    // calculate energy and high frequency content
    // discard 2 lowest bins to avoid unwanted DC component
    // energy = sum_k(|X(k)|^2) for fft bin k
    // HFC = sum_k(k*|X(k)|^2) for fft bin k
    t_float energy = 0.0f;
    t_float hfc = 0.0f;
    t_float mag;
    for(sample = 2; sample < bufferSize; sample+=2)
    {
        /*
        if(sample == 0)
        {
            // to account for ordering in array returned from fft function
            // (mag(re + j*0))^2 = re^2
            mag = *(x->fftBuffer+sample) * *(x->fftBuffer+sample);
        }*/
        // (mag(re + j*im))^2 = re^2 + im^2
        
        // signal reduction
        mag = (*(x->fftBuffer+sample) * *(x->fftBuffer+sample)) + (*(x->fftBuffer+(sample+1)) * *(x->fftBuffer+(sample+1)));
        
        energy += mag;
        hfc += sample*mag;
    }
    
    // peak detection
    if( energy < 1)
        energy = 1; // limit value to avoid divide by zero
    // get measure of transience
    t_float motr = (hfc / x->lastHFC) * (hfc / energy);
    if( hfc < 1 )
        hfc = 1;
    x->lastHFC = hfc;  // limit value to avoid divide by zero

    // check against threshold
    if( motr > x->thresh )
    {
        // check number of frames
        if( x->framesSinceLastTransient >= x->numFrames )
        {
            // we got a transient so let out a bang
            outlet_bang(x->bangOutlet);
            x->framesSinceLastTransient = 0;
        } else
        {
            x->framesSinceLastTransient++;
        }
    } else
    {
        x->framesSinceLastTransient++;
    }
    
    
    return (w+4);
}

// called to start dsp, register perform function
static void tdetectorhfc_dsp(t_tdetectorhfc *x, t_signal **sp)
{
    // save this here because I can't get it to work in the add function...
    x->sr = sp[0]->s_sr;
    // add the perform function
    dsp_add(tdetectorhfc_perform, 3, x, sp[0]->s_vec, sp[0]->s_n);
    
    int bufferSize = sp[0]->s_n;
    
    // set aside space for fft buffer
    x->fftBuffer = (t_float *)malloc(sizeof(t_float) * bufferSize);
    
    
    // zero out fftBuffer
    int i;
    for(i = 0; i < bufferSize; i++)
    {
        *(x->fftBuffer+i) = 0.0f;
    }
    
}


// function to set the number of consecutive frames to disregard a transient
void tdetectorhfc_setNumFrames(t_tdetectorhfc *x, t_floatarg f)
{
    x->numFrames = ( int )f;
}


// clean up memory
static void tdetectorhfc_free(t_tdetectorhfc *x)
{
	free(x->fftBuffer);
}


// new function that is called everytime we create a new tdetectorhfc object in pd
static void *tdetectorhfc_new(void)
{
    // create object from class
    t_tdetectorhfc *x = (t_tdetectorhfc *)pd_new(tdetectorhfc_class);
    
    // create an inlet to set the number of frames to disregard transients
    inlet_new(&x->x_obj, &x->x_obj.ob_pd, gensym("float"), gensym("numFrames"));
    
    // create a bang outlet for when we see a transient
    x->bangOutlet = outlet_new(&x->x_obj, gensym("bang"));
    
    // create a signal outlet for the delayed signal
    //outlet_new(&x->x_obj, gensym("signal"));
    
    x->sr = 0.0f;
    x->lastHFC = 1.0f;
    x->thresh = 0.5f;
    x->numFrames = 2;
    x->framesSinceLastTransient = x->numFrames + 1; // so we catch the first transient
    
    return (x);
}

// called when Pd is first loaded and tells Pd how to load the class
void tdetectorhfc_tilde_setup(void)
{
    tdetectorhfc_class = class_new(gensym("tdetectorhfc~"), (t_newmethod)tdetectorhfc_new, (t_method)tdetectorhfc_free, sizeof(t_tdetectorhfc), 0, 0);
    
    // magic to declare the leftmost inlet the main inlet that will take a signal
    // installs delayTime as the leftmost inlet float
    CLASS_MAINSIGNALIN(tdetectorhfc_class, t_tdetectorhfc, thresh);
    
    // register method that will be called when dsp is turned on
    class_addmethod(tdetectorhfc_class, (t_method)tdetectorhfc_dsp, gensym("dsp"), (t_atomtype)0);
    
    // register callback method for right inlet for number of frames to disregard (float)
    class_addmethod(tdetectorhfc_class, (t_method)tdetectorhfc_setNumFrames, gensym("numFrames"), A_FLOAT, 0);
    
}


/* ============================ */
// FFT METHODS FROM MUSICDSP.ORG
/* ============================ */

/////////////////////////////////////////////////////////
// Sorensen in-place split-radix FFT for real values
// data: array of doubles:
// re(0),re(1),re(2),...,re(size-1)
//
// output:
// re(0),re(1),re(2),...,re(size/2),im(size/2-1),...,im(1)
// normalized by array length
//
// Source:
// Sorensen et al: Real-Valued Fast Fourier Transform Algorithms,
// IEEE Trans. ASSP, ASSP-35, No. 6, June 1987

void realfft_split(float *data,long n){
    
    long i,j,k,i5,i6,i7,i8,i0,id,i1,i2,i3,i4,n2,n4,n8;
    double t1,t2,t3,t4,t5,t6,a3,ss1,ss3,cc1,cc3,a,e,sqrt2;
    
    sqrt2=sqrt(2.0);
    n4=n-1;
    
    //data shuffling
    for (i=0,j=0,n2=n/2; i<n4 ; i++){
        if (i<j){
            t1=data[j];
            data[j]=data[i];
            data[i]=t1;
        }
        k=n2;
        while (k<=j){
            j-=k;
            k>>=1;
        }
        j+=k;
    }
	
    /*----------------------*/
	
	//length two butterflies
	i0=0;
	id=4;
    do{
        for (; i0<n4; i0+=id){
			i1=i0+1;
			t1=data[i0];
			data[i0]=t1+data[i1];
			data[i1]=t1-data[i1];
		}
        id<<=1;
        i0=id-2;
        id<<=1;
    } while ( i0<n4 );
    
    /*----------------------*/
    //L shaped butterflies
    n2=2;
    for(k=n;k>2;k>>=1){
        n2<<=1;
        n4=n2>>2;
        n8=n2>>3;
        e = 2.0f*M_PI/(n2);
        i1=0;
        id=n2<<1;
        do{
            for (; i1<n; i1+=id){
                i2=i1+n4;
                i3=i2+n4;
                i4=i3+n4;
                t1=data[i4]+data[i3];
                data[i4]-=data[i3];
                data[i3]=data[i1]-t1;
                data[i1]+=t1;
                if (n4!=1){
                    i0=i1+n8;
                    i2+=n8;
                    i3+=n8;
                    i4+=n8;
                    t1=(data[i3]+data[i4])/sqrt2;
                    t2=(data[i3]-data[i4])/sqrt2;
                    data[i4]=data[i2]-t1;
                    data[i3]=-data[i2]-t1;
                    data[i2]=data[i0]-t2;
                    data[i0]+=t2;
                }
            }
            id<<=1;
            i1=id-n2;
            id<<=1;
        } while ( i1<n );
        a=e;
        for (j=2; j<=n8; j++){
            a3=3*a;
            cc1=cos(a);
            ss1=sin(a);
            cc3=cos(a3);
            ss3=sin(a3);
            a=j*e;
            i=0;
            id=n2<<1;
            do{
                for (; i<n; i+=id){
                    i1=i+j-1;
                    i2=i1+n4;
                    i3=i2+n4;
                    i4=i3+n4;
                    i5=i+n4-j+1;
                    i6=i5+n4;
                    i7=i6+n4;
                    i8=i7+n4;
                    t1=data[i3]*cc1+data[i7]*ss1;
                    t2=data[i7]*cc1-data[i3]*ss1;
                    t3=data[i4]*cc3+data[i8]*ss3;
                    t4=data[i8]*cc3-data[i4]*ss3;
                    t5=t1+t3;
                    t6=t2+t4;
                    t3=t1-t3;
                    t4=t2-t4;
                    t2=data[i6]+t6;
                    data[i3]=t6-data[i6];
                    data[i8]=t2;
                    t2=data[i2]-t3;
                    data[i7]=-data[i2]-t3;
                    data[i4]=t2;
                    t1=data[i1]+t5;
                    data[i6]=data[i1]-t5;
                    data[i1]=t1;
                    t1=data[i5]+t4;
                    data[i5]-=t4;
                    data[i2]=t1;
                }
                id<<=1;
                i=id-n2;
                id<<=1;
            } while(i<n);
        }
    }
    
	//division with array length
    for(i=0;i<n;i++) data[i]/=n;
}


/////////////////////////////////////////////////////////
// Sorensen in-place inverse split-radix FFT for real values
// data: array of doubles:
// re(0),re(1),re(2),...,re(size/2),im(size/2-1),...,im(1)
//
// output:
// re(0),re(1),re(2),...,re(size-1)
// NOT normalized by array length
//
// Source:
// Sorensen et al: Real-Valued Fast Fourier Transform Algorithms,
// IEEE Trans. ASSP, ASSP-35, No. 6, June 1987

void irealfft_split(float *data,long n)
{
    
    long i,j,k,i5,i6,i7,i8,i0,id,i1,i2,i3,i4,n2,n4,n8,n1;
    double t1,t2,t3,t4,t5,a3,ss1,ss3,cc1,cc3,a,e,sqrt2;
    
    sqrt2=sqrt(2.0);
    
    n1=n-1;
    n2=n<<1;
    for(k=n;k>2;k>>=1){
        id=n2;
        n2>>=1;
        n4=n2>>2;
        n8=n2>>3;
        e = 2.0f*M_PI/(n2);
        i1=0;
        do{
            for (; i1<n; i1+=id){
                i2=i1+n4;
                i3=i2+n4;
                i4=i3+n4;
                t1=data[i1]-data[i3];
                data[i1]+=data[i3];
                data[i2]*=2;
                data[i3]=t1-2*data[i4];
                data[i4]=t1+2*data[i4];
                if (n4!=1){
                    i0=i1+n8;
                    i2+=n8;
                    i3+=n8;
                    i4+=n8;
                    t1=(data[i2]-data[i0])/sqrt2;
                    t2=(data[i4]+data[i3])/sqrt2;
                    data[i0]+=data[i2];
                    data[i2]=data[i4]-data[i3];
                    data[i3]=2*(-t2-t1);
                    data[i4]=2*(-t2+t1);
                }
            }
            id<<=1;
            i1=id-n2;
            id<<=1;
        } while ( i1<n1 );
        a=e;
        for (j=2; j<=n8; j++){
            a3=3*a;
            cc1=cos(a);
            ss1=sin(a);
            cc3=cos(a3);
            ss3=sin(a3);
            a=j*e;
            i=0;
            id=n2<<1;
            do{
                for (; i<n; i+=id){
                    i1=i+j-1;
                    i2=i1+n4;
                    i3=i2+n4;
                    i4=i3+n4;
                    i5=i+n4-j+1;
                    i6=i5+n4;
                    i7=i6+n4;
                    i8=i7+n4;
                    t1=data[i1]-data[i6];
                    data[i1]+=data[i6];
                    t2=data[i5]-data[i2];
                    data[i5]+=data[i2];
                    t3=data[i8]+data[i3];
                    data[i6]=data[i8]-data[i3];
                    t4=data[i4]+data[i7];
                    data[i2]=data[i4]-data[i7];
                    t5=t1-t4;
                    t1+=t4;
                    t4=t2-t3;
                    t2+=t3;
                    data[i3]=t5*cc1+t4*ss1;
                    data[i7]=-t4*cc1+t5*ss1;
                    data[i4]=t1*cc3-t2*ss3;
                    data[i8]=t2*cc3+t1*ss3;
                }
                id<<=1;
                i=id-n2;
                id<<=1;
            } while(i<n1);
        }
	}	
    
    /*----------------------*/
	i0=0;
	id=4;
    do{
        for (; i0<n1; i0+=id){ 
			i1=i0+1;
			t1=data[i0];
			data[i0]=t1+data[i1];
			data[i1]=t1-data[i1];
		}
        id<<=1;
        i0=id-2;
        id<<=1;
    } while ( i0<n1 );
    
    /*----------------------*/
    
    //data shuffling
    for (i=0,j=0,n2=n/2; i<n1 ; i++){
        if (i<j){
            t1=data[j];
            data[j]=data[i];
            data[i]=t1;
        }
        k=n2;
        while (k<=j){
            j-=k;
            k>>=1;	
        }
        j+=k;
    }	
}


/////////////////////////////////////////////////////////
// Sorensen in-place split-radix FFT for real values
// data: array of doubles:
// re(0),re(1),re(2),...,re(size-1)
//
// output:
// re(0),re(size/2),re(1),im(1),re(2),im(2),...,re(size/2-1),im(size/2-1)
// normalized by array length
//
// Source:
// Source: see the routines it calls ...

void realfft_split_unshuffled(float *data,long n)
{
    
	float *data2;
	long i,j;
    
	realfft_split(data,n);
	//unshuffling - not in-place
	data2=(float *)malloc(n*sizeof(float));
	j=n/2;
	data2[0]=data[0];
	data2[1]=data[j];
	for(i=1;i<j;i++) {data2[i+i]=data[i];data2[i+i+1]=data[n-i];}
	for(i=0;i<n;i++) data[i]=data2[i];
	free(data2);
}



